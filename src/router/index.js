import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import CarList from '@/components/CarList'
import ShowCar from '@/components/ShowCar'
import CreateCar from '@/components/CreateCar'
import EditCar from '@/components/EditCar'
import OB from '@/components/OB'
import CarListAll from '@/components/CarListAll'
import CarsSound from '@/components/CarsSound'
import CarListAll1 from '@/components/CarListAll1'
import EmployeeList from '@/components/employee/EmployeeList'
import ShowEmployee from '@/components/employee/ShowEmployee'
import CreateEmployee from '@/components/employee/CreateEmployee'
import EditEmployee from '@/components/employee/EditEmployee'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/list',
      name: 'CarListAll',
      component: CarListAll
    },
    {
      path: '/listA',
      name: 'CarListAll1',
      component: CarListAll1
    },
    {
      path: '/list-cars',
      name: 'CarList',
      component: CarList
    },
    {
      path: '/sound/:id',
      name: 'CarsSound',
      component: CarsSound
    },
    {
      path: '/ob',
      name: 'OB',
      component: OB
    },
    {
      path: '/show-car/:id',
      name: 'ShowCar',
      component: ShowCar
    },
    {
      path: '/add-car',
      name: 'CreateCar',
      component: CreateCar
    },
    {
      path: '/edit-car/:id',
      name: 'EditCar',
      component: EditCar
    },
    {
      path: '/list-employees',
      name: 'EmployeeList',
      component: EmployeeList
    },
    {
      path: '/show-employee/:id',
      name: 'ShowEmployee',
      component: ShowEmployee
    },
    {
      path: '/add-employee',
      name: 'CreateEmployee',
      component: CreateEmployee
    },
    {
      path: '/edit-employee/:id',
      name: 'EditEmployee',
      component: EditEmployee
    }
  ]
})
