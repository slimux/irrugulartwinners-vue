// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App'
import router from './router'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VModal from 'vue-js-modal'
import store from '@/store/store'
import VuePaginate from 'vue-paginate'
import VuejsDialog from "vuejs-dialog"
import VueHighcharts from 'vue2-highcharts'
import Highcharts from 'highcharts'



Vue.use(VModal, { dialog: true })
Vue.use(VueHighcharts, { Highcharts })
Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(VuePaginate)
Vue.use(VuejsDialog)
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
