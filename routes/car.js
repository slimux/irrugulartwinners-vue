var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Car = require('../models/Car.js');

/* GET ALL Car */
router.get('/', function(req, res, next) {
  Car.find(function (err, products) {
    if (err) return next(err);
    res.json(products);
  });
});


/* GET SINGLE Car BY ID */
router.get('/:id', function(req, res, next) {
  Car.findById(req.params.id, function (err, post) {
    if (err) return next(err);
    res.status(200).json(post);
  });
});

/* SAVE Car */
router.post('/', function(req, res, next) {
  Car.create(req.body, function (err, post) {
    if (err) { 
      res.send(err)
    }
    res.json(post);
  });
});

/* UPDATE Car */
router.put('/:id', function(req, res, next) {
  console.log(req.body);
  Car.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

/* DELETE Car */
router.delete('/:id', function(req, res, next) {
  Car.findByIdAndRemove(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});


/* GET SINGLE Car BY carname */
router.get('/:carname', function(req, res, next) {
  Car.findBycarname(req.params.carname, function (err, post) {
    if (err) return next(err);
    res.status(200).json(post);
  });
}); 



router.get('/:engine', function(req, res, next){
  Car.find({state: req.params.state}, function(err, result){
    if(err)
      res.send(err);
    else{
      res.json(result);
    }
  })
})


router.put('/traiter/:id', function(req, res, next){
  console.log('HI!');
  Car.findOne({_id: req.params.id}, function(err, result){
    if(err)
      res.send(err);
    else{
      let carToUpdate = new Car(result);
      console.log(carToUpdate)
      carToUpdate.engine = true;
      console.log(carToUpdate)
      carToUpdate.save(function(err, result){
        if(err){
                res.send(err);
            }
            console.log(result)
            res.send(result)
    
      });
    }
  })
});




router.put('/traiter1/:id', function(req, res, next){
  console.log('HI!');
  Car.findOne({_id: req.params.id}, function(err, result){
    if(err)
      res.send(err);
    else{
      let carToUpdate = new Car(result);
      carToUpdate.engine = false;
      carToUpdate.save(function(err, result){
        if(err){
                res.send(err);
            }
            res.send(result)
         
      });
    }
  })
});


module.exports = router;
