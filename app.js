var express = require('express')
var path = require('path')
var favicon = require('serve-favicon')
var logger = require('morgan')
var bodyParser = require('body-parser')
var cors = require('cors')
var car = require('./routes/car')
var employee = require('./routes/employee')
var users = require('./routes/user')
var app = express()

var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
mongoose.connect('mongodb://mahdi:j1ofjVuh30NKD87I@mevn-stack-shard-00-00-spkmy.mongodb.net:27017,mevn-stack-shard-00-01-spkmy.mongodb.net:27017,mevn-stack-shard-00-02-spkmy.mongodb.net:27017/mevn-stack?ssl=true&replicaSet=mevn-stack-shard-0&authSource=admin')
.then(() =>  console.log('connection succesful'))
  .catch((err) => console.error(err))

app.use(cors())
app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({'extended':'false'}))
app.use(express.static(path.join(__dirname, 'dist')))
app.use('/cars', express.static(path.join(__dirname, 'dist')))
app.use('/car', car)
app.use('/employees',employee)
app.use('/users', users)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in developmen
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
