var mongoose = require('mongoose');

var EmployeeSchema = new mongoose.Schema({
  firstname: String,
  lastname: String,
  email: String,
  picture: String,
  date_naissance:  { type: Date  },
  user_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'credentials',
    required: false
  },
});

module.exports = mongoose.model('Employee', EmployeeSchema);
