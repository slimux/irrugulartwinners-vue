var mongoose = require('mongoose');

var CarSchema = new mongoose.Schema({
  isbn: String,
  carname: String,
  carbrand: String,
  description: String,
  creation_year:  { type: Date },
  available: {
    type: Boolean,
    default: false
  },
  engine: {
    type: Boolean,
    default: false
  },
  updated_date: { type: Date, default: Date.now },
    client: {
    type: String,
    required: false
  },
    client_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'client',
    required: false
  },
   energylevel: {
    type: Number, 
    default: 500 }
});

module.exports = mongoose.model('Car', CarSchema);
